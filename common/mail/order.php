<?php
/* @var $order common\models\Order */
use yii\helpers\Html;
?>


<h1>Здравствуйте, <?= Html::encode($order->fio) ?></h1>
<h3>
    Вы сделали заказ на сайте debora-cosmetics.com<br>
    Номер вашего заказа #<?= Html::encode($order->id) ?>
</h3>

<ul>
    <li>Телефон: <?= Html::encode($order->phone) ?></li>
    <li>Email:   <?= Html::encode($order->email) ?></li>
</ul>

<h3>Вы заказали:</h3>

<ul>
    <?php $sum = 0; ?>
    <?php foreach ($order->orderItems as $item): ?>
        <?php $sum += $item->quantity * $item->price ?>
        <li>
            <?= Html::encode(
                    $item->title .
                    ' x ' .
                    $item->quantity .
                    ' x ' . 
                    Yii::$app->formatter->asInteger($item->price) .
                    'руб.'
            ) ?>
        </li>
    <?php endforeach ?>
</ul>

<p><string>Итого: </string> <?php echo Yii::$app->formatter->asInteger($sum)?>руб.</p>

