<?php
/* @var $order common\models\Order */
use yii\helpers\Html;
?>

<h1>Здравствуйте</h1>
<h3>Вы успешно подписаны на новости от Debora-Cosmetics.</h3>

<p>Вы успешно подписаны на новости от Debora-Cosmetics</p>
<p>Администрация интернет-магазина Debora-Cosmetics благодарит Вас за оформление интернет-подписки</br>
на Новости от нашего магазина и дарит Вам КУПОН на скидку в размере 10%.</br>
Для получения скидки, при покупке введите намер Вашего купона: <b><?=Html::encode($model->code)?></b> в соответствующее поле ввода.</p>
<p>Для отписки от рассылки перейдите по адресу: 
    <a href="http://test.foxinweb.bget.ru/otpiska-ot-rassylki/<?=Html::encode(\common\models\Subscribe::makeResubId($model->id))?>/">Отписаться от рассылки</a></p>

