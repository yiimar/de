<?php

$hostInfo  = $_SERVER["REQUEST_SCHEME"] . '://' . $_SERVER["SERVER_NAME"] . '/';

Yii::setAlias('common',             dirname(__DIR__));
Yii::setAlias('frontend',           dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend',            dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console',            dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('uploadPath',         '@backend/web/upload/');
Yii::setAlias('productUploadPath',  '@backend/web/upload/product/');
Yii::setAlias('sliderUploadPath',   '@backend/web/upload/slider/');
Yii::setAlias('pageUploadPath',     '@backend/web/upload/page/');
Yii::setAlias('bannerUploadPath',   '@backend/web/upload/banner/');


Yii::setAlias('frontendWebroot', $hostInfo);//
Yii::setAlias('backendWebroot',  $hostInfo . '/admin/');
Yii::setAlias('productUpload',   $hostInfo . '/admin/upload/product/');
Yii::setAlias('sliderUpload',    $hostInfo . '/admin/upload/slider/');
Yii::setAlias('pageUpload',      $hostInfo . '/admin/upload/page/');
Yii::setAlias('bannerUpload',    $hostInfo . '/admin/upload/banner/');
