<?php

namespace common\models\engine;

use Yii;
use common\models\Node;

/**
 * Description of NodeArray
 *
 * @author yiimar
 */
trait NodeTrait
{
    /**
     * Получение нодов дочернего уровня
     * 
     * @param type $id
     * @return type array
     */
    public static function getSubLevel($id)
    {
        return self::find()
                ->where(['parent_id' => $id])
                ->all();        
    }

    /**
     * Получение всех нодов дочерней ветви, включая подветви
     * Массив используется для построения многоуровневых меню
     * 
     * @param type $id
     */
    public static function getBranch($id)
    {
        $branch = [];
        $nodes = self::getSubLevel($id);
        if (!empty($nodes)) {
            foreach ($nodes as $node) {
                $branch[$node->id] = self::getBranch($node->id);                
            }
        }
        return $branch;
    }

    /**
     * Получает массив определенных родителей для узла типа:
     *  [
     *      id, //свой
     *      id, //родителя
     *      id, //деда
     *      ...,
     *      id, //пра-пра-... деда
     *  ]
     * Этот массив используется для формирования исходных массивов для breadcrums
     * 
     * @staticvar array $line
     * @param type $id
     * @return type array
     */
    public static function getParentLine($id)
    {
        static $line = [];

        if (isset($id)) {
            $line[] = $id;
            $node = Node::find()->where(['id' => $id])->one();
            if ($node) {
                self::getParentLine($parent_id);
            }
        }

        return $line;
    }
}
