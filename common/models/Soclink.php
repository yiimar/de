<?php

namespace common\models;

use Yii;
use common\components\SimpleStatusAR;

/**
 * This is the model class for table "{{%social_link}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $url
 * @property string $class
 * @property string $status
 */
class Soclink extends SimpleStatusAR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%social_link}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['label', 'url', 'class'], 'required'],
            [['label', 'url', 'class'], 'string', 'max'   => 255],
            [['status'],                'in',     'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE,]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label'  => 'Наименование',
            'url'    => 'Ссылка',
            'class'  => 'Класс стиля',
            'status' => 'Статус',
        ];
    }
}
