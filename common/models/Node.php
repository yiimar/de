<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%node}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $page_id
 */
class Node extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%node}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'page_id'], 'integer'],
            [['page_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'parent_id' => 'Родительская страница',
            'page_id'   => 'Страница',
        ];
    }
}
