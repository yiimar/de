<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $number
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $notes
 * @property string $status
 * @property string $fio
 * @property integer $total
 * @property integer $total_with
 * @property integer $rebate
 *
 * @property OrderItem[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW        = 'Новый';
    const STATUS_IN_PROGRESS = 'В работе';
    const STATUS_IN_STOCK    = "На складе";
    const STATUS_TO_DELIVERY = "В доставке";
    const STATUS_REFUSAL     = "Отказ";
    const STATUS_DONE       = 'Закрыт';

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone',  'email', 'fio'],                'required'],
            [['status','notes',  'address', 'number'],  'string'],
            [['phone',  'email', 'fio'],                'string',   'max' => 128],
            [['email'],                                 'email'],
            [['rebate', 'total', 'total_with'],         'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'number'     => 'Номер заказа',
            'total'      => 'Сумма',
            'total_with' => 'Со скидкой',
            'created_at' => 'Cоздан',
            'updated_at' => 'Изменен',
            'phone'      => 'Телефон',
            'address'    => 'Адрес',
            'email'      => 'Email',
            'rebate'     => 'Скидка',
            'notes'      => 'Описание заказа',
            'status'     => 'Состояние заказа',
            'fio'        => 'ФИО',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status = self::STATUS_NEW;
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_IN_PROGRESS,
            self::STATUS_IN_STOCK,
            self::STATUS_TO_DELIVERY,
            self::STATUS_DONE,
            self::STATUS_REFUSAL,
        ];
    }

    public static function statusForDrop()
    {
        $res = [];
        $arr = self::getStatuses();
        foreach ($arr as $item) :
            $res[] = [
                'status' => $item,
                'title'  => $item,
            ];
        endforeach;
        unset($arr);
        
        return ArrayHelper::map($res, 'status', 'title');
    }

    public function sendEmail()
    {
        return Yii::$app->mailer->compose('order', ['order' => $this])
            ->setTo($this->email)
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setSubject('Новый заказ #' . $this->id)
            ->send();
    }
}
