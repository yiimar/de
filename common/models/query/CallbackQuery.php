<?php

namespace common\models\query;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Callback;

/**
 * CallbackQuery represents the model behind the search form about `common\models\Callback`.
 */
class CallbackQuery extends Callback
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone'],             'string',   'max' => 20,],
            [['created', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Callback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'phone' => $this->phone,
            'created' => $this->created,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
