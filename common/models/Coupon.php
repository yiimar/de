<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%coupon}}".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $code
 * @property integer $rebate
 * @property string $recipient
 * @property integer $created
 * @property integer $closed
 *
 * @property Action $action
 */
class Coupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%coupon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'code', 'rebate', 'recipient',], 'required'],
            [['action_id', 'rebate', 'created', 'closed'], 'integer'],
            [['recipient', 'code',], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Вид скидки',
            'code'      => 'Код купона',
            'rebate'    => 'Скидка, %',
            'recipient' => 'Получатель',
            'created'   => 'Дата создания',
            'closed'    => 'Дата закрытия',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    public static function makeCode($action, $email)
    {
        $model = new Coupon();
        $min = 0;   $max = 9;
        $str = '';

        for ($i = 0; $i < 10; $i++)             $str .= rand($min, $max);

        $model->code      = $str;
        $model->action_id = $action;
        $model->recipient = $email;
        $model->rebate    = Rebate::getRebateActual($action);
        $model->save();
        
        return $str;
    }

    public static function getCouponByCode($promo)
    {
        return self::find()
                ->where(['code' => $promo,])
                ->one();
    }

    public static function getRebateByPromo($promo)
    {
        $model = self::find()
                ->where(['code' => $promo])
                ->andWhere(['closed' => null])
                ->one();

        if ($model && $model->closed === null)          return $model->rebate;
        else                                            return false;
    }

    public static function makeClose($promo)
    {
        $model = self::getCouponByCode($promo);
        $model->closed = time();
        
        return $model->save(false);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created = time();
            }
        }
        return true;
    }
}
