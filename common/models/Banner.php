<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\components\SimpleStatusAR;

/**
 * This is the model class for table "{{%banner}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $pict
 * @property integer $width
 * @property integer $height
 * @property string $class
 * @property integer $place_page_id
 * @property integer $target_place_id
 * @property string $target
 * @property string $status
 */
class Banner extends SimpleStatusAR
{
    /**
     * File upload field
     * 
     * @var UploadedFile
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['width', 'height', 'place_page_id', 'target_place_id'], 'integer'],
            [['title', 'pict', 'target'],   'string', 'max'   => 255],
            [['class'],                     'string', 'max'   => 100],
            [['status'],                    'in',     'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE,]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'           => 'Наименование',
            'pict'            => 'Картинка',
            'file'            => 'Загрузить картинку',
            'width'           => 'Ширина',
            'height'          => 'Высота',
            'class'           => 'Класс стиля',
            'place_page_id'   => 'Посадочная страница',
            'target_place_id' => 'Страница назначения',
            'target'          => 'Целевая ссылка',
            'status'          => 'Статус',
        ];
    }
}
