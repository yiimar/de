<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%action}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 */
class Action extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'       => 'Наименование',
            'description' => 'Описание',
        ];
    }

    public static function getDescriptionById($id)
    {
        $model = self::findOne($id);
//                ->where(['id' => $id])
//                ->one();
        return $model->description;
    }

    public static function getActionList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'description');
    }
}
