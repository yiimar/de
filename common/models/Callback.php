<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%callback}}".
 *
 * @property integer $id
 * @property integer $phone
 * @property integer $created
 * @property integer $status
 */
class Callback extends \yii\db\ActiveRecord
{
    const STATUS_NEW        = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_DONE       = 3;

    const STATUS_NEW_TEXT        = 'Новый';
    const STATUS_IN_PROGRESS_TEXT = 'В работе';
    const STATUS_DONE_TEXT       = 'Закрыт';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%callback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone',],            'required'],
            [['phone'],             'string',    'max' => 20,],
            [['created', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'phone'   => 'Телефон',
            'created' => 'Открыт',
            'status'  => 'Статус',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created = time();
                $this->status  = self::STATUS_NEW;
            }
        }
        return true;
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_DONE       => self::STATUS_DONE_TEXT,
            self::STATUS_IN_PROGRESS => self::STATUS_IN_PROGRESS_TEXT,
            self::STATUS_NEW        => self::STATUS_NEW_TEXT
        ];
    }

    public static function getStatusById($id)
    {
        return self::getStatuses()[$id];
    }

    public static function getStasusesList()
    {
        return [
            [
                'status' => self::STATUS_NEW,
                'title'  => self::STATUS_NEW_TEXT,
            ],
            [
                'status' => self::STATUS_IN_PROGRESS,
                'title'  => self::STATUS_IN_PROGRESS_TEXT,
            ],
            [
                'status' => self::STATUS_DONE,
                'title'  => self::STATUS_DONE_TEXT,
            ],
        ];
    }
}
