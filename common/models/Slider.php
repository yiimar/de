<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use common\components\SimpleStatusAR;

/**
 * This is the model class for table "{{%slider}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $slide
 * @property integer $page_id
 * @property integer $status
 */
class Slider extends SimpleStatusAR
{
    /**
     * File upload field
     * 
     * @var UploadedFile
     */
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%slider}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'],           'integer'],
            [['title', 'slide',],   'string',   'max'   => 100],
            [['status'],            'in',       'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE,]],
            [['file'],              'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'title'   => 'Название',
            'slide'   => 'Картинка слайдера',
            'file'    => 'Загрузить Картинку',
            'page_id' => 'Страница',
            'status'  => 'Статус',
        ];
    }
}
