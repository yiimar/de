<?php

namespace common\models;

use Yii;
use common\components\SimpleStatusAR;

/**
 * This is the model class for table "{{%rebate}}".
 *
 * @property integer $id
 * @property integer $action_id
 * @property integer $value
 * @property string $status
 * @property integer $created
 * @property integer $closed
 */
class Rebate extends SimpleStatusAR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rebate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'value', 'status',],  'required'],
            [['action_id', 'value', 'created',], 'integer'],
            [['status'],                         'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE,]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Вид скидки',
            'value'     => 'Размер скидки, %',
            'status'    => 'Статус',
            'created'   => 'Дата создания',
            'closed'    => 'Дата закрытия',
        ];
    }

    public static function getRebateActual($action_id)
    {
        $model = self::find()
                ->where(['action_id' => $action_id])
                ->andWhere(['status' => self::STATUS_ACTIVE,])
                ->one();
        return $model->value;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created = time();
                return true;
            }
        }
        return false;
    }
}
