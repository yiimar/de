<?php

namespace common\models;

/**
 * Description of Category
 *
 * @author yiimar
 */
class Category
{
    const PASHMINA = 'Пашмина';
    const SILK      = 'Из шелка';
    const CASHMERE = 'Кашемир';
    const WOOL     = 'Шерсть';
    
    const PASHMINA_SLUG = 'pashmina';
    const SILK_SLUG      = 'shelk';
    const CASHMERE_SLUG = 'kashemir';
    const WOOL_SLUG     = 'sherst';

    public static function getSlugArray()
    {
        return [
            self::PASHMINA => self::PASHMINA_SLUG,
            self::SILK      => self::SILK_SLUG,
            self::CASHMERE => self::CASHMERE_SLUG,
            self::WOOL     => self::WOOL_SLUG,
        ];
    }

    public static function getSlugById($id)
    {
        return in_array($id, array_keys(self::getSlugArray()))
                ? self::getSlugArray()[$id]
                : self::getSlugArray()[self::PASHMINA];
    }

    public static function getSeriaArray()
    {
        return [
            self::PASHMINA => self::PASHMINA_SLUG,
            self::SILK      => self::SILK_SLUG,
            self::CASHMERE => self::CASHMERE_SLUG,
            self::WOOL     => self::WOOL_SLUG,
        ];
    }

    public static function getSeriaById($id)
    {
        return in_array($id, array_keys(self::getSeriaArray()))
                ? self::getSeriaArray()[$id]
                : self::getSeriaById(self::PASHMINA);
    }

    public static function getIDBySeria($seria)
    {
        $arr = array_flip(self::getSeriaArray());
        
        return in_array($seria, array_keys($arr))
                ? $arr[$seria]
                : $arr[self::PASHMINA_SLUG];
        
    }

    public static function getSeriaList()
    {
        return [
            [
                'id'    => self::PASHMINA,
                'title' => self::PASHMINA,
            ],
            [
                'id'    => self::SILK,
                'title' => self::SILK,
            ],
            [
                'id'    => self::PASHMINA,
                'title' => self::PASHMINA,
            ],
            [
                'id'    => self::WOOL,
                'title' => self::WOOL,
            ],
        ];
    }

    public static function makeSlug($category)
    {
        $seria = [];
        
        switch ($category) :
            case self::PASHMINA_SLUG:
            {
                $seria = [
                    self::PASHMINA,
                ];
            }
            break;
        
            case self::SILK_SLUG:
            {
                $seria = [
                    self::SILK,
                ];
            }
            break;
            
            case self::CASHMERE:
            {
                $seria = [
                    self::CASHMERE,
                ];
            }
            break;
           
            case self::WOOL_SLUG:
            {
                $seria = [
                    self::WOOL,
                ];
            }
            break;

            default :
            {
                $seria = [
                    self::PASHMINA,
                ];
            }
        endswitch;
        
        return $seria;
    }
}
