<?php

namespace common\models;

use Yii;
use common\components\SimpleStatusAR;

/**
 * This is the model class for table "{{%option}}".
 *
 * @property integer $id
 * @property string $attribute
 * @property string $value
 * @property string $status
 */
class Option extends SimpleStatusAR
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
                [
                    [['attribute', 'value'], 'string', 'max' => 100],
                ],
                parent::rules()
        );
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute' => 'Атрибут',
            'value'     => 'Значение',
            'status'    => 'Статус',
        ];
    }
}
