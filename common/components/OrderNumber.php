<?php

namespace common\components;

/**
 * Description of OrderNumber
 *
 * @author yiimar
 */
class OrderNumber
{
    const DEPARTAMENT_CODE = '01';
    const NUMBER_SHIFT      = 9;
    const COUNT_ORIGIN      = 97;

    public static function makeNumber($id)
    {
        return ''
                . self::DEPARTAMENT_CODE
                . self::makeYear()
                . self::COUNT_ORIGIN
                . self::makeMonth()
                . self::makeId($id)
                . self::makeRandom();
    }

    public static function makeYear()
    {
        return substr(date('y') + self::NUMBER_SHIFT, 1);
    }

    public static function makeMonth()
    {
        return '' . (date('n') + self::NUMBER_SHIFT);
    }

    public static function makeId($id)
    {
        return substr('000' . $id, -3);
    }

    public static function makeRandom()
    {
        return rand(1, 9) . rand(0, 9);
    }
}
