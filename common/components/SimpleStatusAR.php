<?php

namespace common\components;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Description of SimpleStatusAR
 *
 * @author yiimar
 */
class SimpleStatusAR extends ActiveRecord
{
    const STATUS_ACTIVE  = 'Активен';
    const STATUS_DISABLE = 'Не активен';

    public static function Statuses()
    {
        return [
            [self::STATUS_ACTIVE],
            [self::STATUS_DISABLE],
        ];
    }

    public static function getStatuses()
    {
        return [
            ['id' => self::STATUS_ACTIVE,  'title' => self::STATUS_ACTIVE],
            ['id' => self::STATUS_DISABLE, 'title' => self::STATUS_DISABLE],
        ];
    }
    
    public static function statusForDrop()
    {
        return ArrayHelper::map(self::getStatuses(), 'id', 'title');
    }

    public function rules()
    {
        return [
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE]]
        ];
    }
}