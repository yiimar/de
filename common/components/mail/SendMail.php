<?php

namespace common\components\mail;

use Yii;
use common\components\mail\SendMailSmtpClass;

/**
 * Description of SendMail component class
 *
 * @author yiimar
 * 
 * @param str $to
 * @param str $subject
 * @param str $message
 * @param str $fromname
 * 
 * Called by-   
 *              SendMail::send( $email , $subject, $message, $from);
 */
class SendMail 
{
    static function send($to, $subject, $message, $from="Администрация интернет-магазина Debora-Cosmetics") 
    {
        $smtp= Yii::$app->params['smtp'];
//        Yii::import('ext.smtpmail.SendMailSmtpClass');
        $mailSMTP = new SendMailSmtpClass($smtp['username'] . '@yandex.ru', $smtp['password'], $smtp['host'], $from, $smtp['port']);
        // $mailSMTP = new SendMailSmtpClass('логин', 'пароль', 'хост', 'имя отправителя', 465);        
        
        // заголовок письма
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
        $headers .= "From:" . $from . "\r\n"; // от кого письмо
        
        
        $result =  $mailSMTP->send($to, $subject, $message, $headers); // отправляем письмо
        // $result =  $mailSMTP->send('Кому письмо', 'Тема письма', 'Текст письма', 'Заголовки письма');      
        return $result;
    }
}