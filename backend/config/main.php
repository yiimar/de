<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id'                  => 'Панель Администратора',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap'           => ['log'],
    'modules' => [
    ],
    'defaultRoute'        => 'order',
    'modules'               => [
        'crud' => [
            'class' => 'backend\modules\crud\CrudModule',
        ],
    ],
    'components'          => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require (__DIR__ . '/rules.php'),
        ],
        'request' => [
            'baseUrl' => '/admin'
        ],
        'user' => [
            'identityClass'   => 'backend\models\Admin',
            'enableAutoLogin' => true,

            'identityCookie'  => [
                'name'     => '_backendIdentity',
                'httpOnly' => true,
            ],
        ],
        'authManager' => [
            'class'          => 'yii\rbac\PhpManager',
            'defaultRoles'   => [ 'admin'],//'user',
            'itemFile'       => '@backend/rbac/data/items.php',
            'assignmentFile' => '@backend/rbac/data/assignments.php',
            'ruleFile'       => '@backend/rbac/data/rules.php'
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
//                'path' => '/admin',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
