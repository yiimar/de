<?php

$rules = [
    '/'                                         => 'order/index',
'<_a:(login|logout|signup|email-confirm|request-password-reset|password-reset)>' => 'site/<_a>',

    [
        'pattern' => '<kind:(option|coupon|callback|page|rebate|subscribe|soclink)>',
        'route'   => 'crud/crud/index',
        'suffix'  => '',
    ],
    [
        'pattern' => '<kind:(option|coupon|callback|page|rebate|subscribe|soclink)>/<action>',
        'route'   => 'crud/crud/<action>',
        'suffix'  => '',
    ],
    [
        'pattern' => '<kind:(option|coupon|callback|page|rebate|subscribe|soclink)>/<action>/<id:\d+>',
        'route'   => 'crud/crud/<action>',
        'suffix'  => '',
    ],

//    [
//        'pattern' => '<kind:(slider|banner)>/<action>',
//        'route'   => 'crudupload/<action>',
//        'suffix'  => '',
//    ],
//    [
//        'pattern' => '<kind:(slider|banner)>/<action>/<id:\d+>',
//        'route'   => 'crudUpload/<action>',
//        'suffix'  => '',
//    ],

    '<controller>/<action>/<id:\d+>'            => '<controller>/<action>', 
    '<controller>/<action>'                     => '<controller>/<action>', 

    '<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>'        => '<_c>/<_a>',
    '<_c:[\w\-]+>/<id:\d+>'                     => '<_c>/view',
    '<_c:[\w\-]+>'                              => '<_c>/index',
];

return $rules;
