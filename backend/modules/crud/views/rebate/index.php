<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Скидки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rebate-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать скидку', [$this->context->prePath . 'create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'action_id',
                'value'     => function ($model) {
                    $d = \common\models\Action::findOne(['id' => $model->action_id]);
                    return $d->description;
                }
            ],
            'value',
            'status',
            'created:datetime',
            [
                'attribute' => 'closed',
                'value'     => function ($model) {
                    return isset($model->closed) 
                                        ? Yii::$app->formatter->asDatetime($model->closed)
                                        : '';
                }
            ],
            [
                'class' => 'backend\components\CrudActionColumn',
            ],
        ],
    ]); ?>

</div>
