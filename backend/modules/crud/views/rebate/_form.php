<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Action;
use common\models\Rebate;

/* @var $this yii\web\View */
/* @var $model common\models\Rebate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rebate-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'action_id')->dropDownList(Action::getActionList(), ['prompt' => 'Выбор акции']) ?>

                <?= $form->field($model, 'value')->textInput(['format' => 'integer']) ?>

                <?= $form->field($model, 'status')->dropDownList(Rebate::statusForDrop(), ['prompt' => 'Выбор статуса']) ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Создать скидку' : 'Изменить сидку', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>