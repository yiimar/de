<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Yamap;

/* @var $this yii\web\View */
/* @var $model common\models\Option */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="option-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attribute')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => 100]) ?>

            <?php echo $form->field($model, 'status')->dropDownList(Yamap::statusForDrop()); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
