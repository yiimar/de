<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Option */

$this->title = 'Изменить опцию: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => [$this->context->prePath . 'index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="option-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
