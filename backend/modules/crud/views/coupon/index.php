<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Купоны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-index">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <p>
    <?php // echo Html::a('Создать купон', [$this->context->prePath . 'create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'action_id',
                'value' => function ($model) {
                    return \common\models\Action::getDescriptionById(['id' => $model->action_id]);
                }
            ],
            'code',
            'rebate',
            'recipient',
            'created:datetime',
            [
                'attribute' => 'closed',
                'value' => function ($model) {
                    return ($model->closed === null) ? 'не закрыт' : Yii::$app->formatter->asDatetime($model->closed);
                }
            ],
            [
                'class' => 'backend\components\CrudActionColumn',
            ],
        ],
    ]); ?>

</div>
