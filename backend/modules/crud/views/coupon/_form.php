<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-lg-5">

        <div class="coupon-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'action_id')->dropDownList(\common\models\Action::getActionList(), ['prompt' => 'Выбор акции']) ?>

            <?= $form->field($model, 'code')->textInput() ?>

            <b><?= 'Скидка= ' ?></b> <div id="coupon-form-value"><?= $model->rebate ?></div><br>

            <?= $form->field($model, 'recipient')->textInput(['maxlength' => 100]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
