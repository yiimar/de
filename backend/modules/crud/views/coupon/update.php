<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */

$this->title = 'Изменить купон: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Купоны', 'url' => [$this->context->prePath . 'index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
