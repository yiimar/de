<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SocialLink */

$this->title = 'Изменить социальную ссылку: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Социальные ссылки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-link-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
