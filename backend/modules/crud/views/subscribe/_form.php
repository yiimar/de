<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Subscribe;

/* @var $this yii\web\View */
/* @var $model common\models\Subscribe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscribe-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => 128]) ?>
                <?= $form->field($model, 'status')->dropDownList(Subscribe::statusForDrop()); ?>

                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Подписать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>