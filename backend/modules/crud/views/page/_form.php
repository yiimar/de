<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="page-form">
    <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-lg-5">

                <?php if ($model->id && $model->id < 6) : ?>
                    <?php echo 'Страница ' . $model->title ?>
                <?php else : ?>
                    <?= $form->field($model, 'title')->textInput() ?>
                    <?php echo $form->field($model, 'slug')->textInput() ?>
                <?php endif; ?>

                <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'meta_description')->textInput(['maxlength' => true]) ?>

                <?php if (!($model->id && $model->id < 6)) echo $form->field($model, 'parent_id')->dropDownList(\common\models\Page::getParentList()); ?>
            </div>
        </div>
        <?php echo $form->field($model, 'text')->widget(TinyMce::className(), [
            'fileManager' => [
                'class'          => TinyMceElFinder::className(),
                'connectorRoute' => 'el-finder/connector',
            ],
        ]); ?>
        <div class="row">
            <div class="col-lg-5">
                <?php if (!($model->id && $model->id < 6)) echo $form->field($model, 'status')->dropDownList(\common\models\Page::getStatusesList()); ?>

                <div class="form-group">
                    <?= Html::submitButton(Html::encode('Сохранить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
           </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>