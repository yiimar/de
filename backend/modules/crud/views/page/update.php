<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Html::encode('Редактировать страницу: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Html::encode('Страницы'),    'url' => ['/page/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">

    <h1><?= $this->title ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
