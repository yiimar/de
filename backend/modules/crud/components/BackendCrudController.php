<?php

namespace backend\modules\crud\components;

use Yii;
use yii\data\ActiveDataProvider;
use backend\components\AbstractCrudController;

/**
 * Description of BackendCrudController
 *
 * @author yiimar
 */
class BackendCrudController extends AbstractCrudController
{
    /**
     * Setting path alias for views
     * @return type string
     */
    public function getViewPath()
    {
        return $this->module->viewPath . DIRECTORY_SEPARATOR . $this->kind;
    }

    /**
     * Lists all Option models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelClass = $this->modelClass;

        $dataProvider = new ActiveDataProvider([
            'query' => $modelClass::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Option model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->modelClass;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([$this->prePath . 'index',]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Option model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([$this->prePath . 'index',]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Option model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect([$this->prePath . 'index']);
    }

}
