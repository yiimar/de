<?php

namespace backend\modules\crud;

class CrudModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\crud\controllers';

    public function init()
    {
        parent::init();
//        $this->defaultRoute = 'crud';
    }
}
