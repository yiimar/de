<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Page;
use backend\components\CrudActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Баннер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'title',
            [
                'attribute' => 'pict',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return HTML::img(
                            Yii::getAlias('@bannerUpload/' . $model->pict),
                            ['width'=>'100px', 'height'=>'auto']
                    );
                },
            ],
            'width',
            'height',
            'class',
            [
                'attribute' => 'place_page_id',
                'value'     => function ($model) {
                    return empty($model->place_page_id) ? 'Не задана' : HTML::encode(Page::getTitleById($model->place_page_id));
                },
            ],
            [
                'attribute' => 'target_place_id',
                'value'     => function ($model) {
                    return empty($model->target_place_id) ? 'Не задана' : HTML::encode(Page::getTitleById($model->target_place_id));
                },
            ],
            'target',
            'status',

            [
//                'class'      => '\\backend\\components\\CrudActionColumn($this->context->kind)',
            ],
        ],
    ]); ?>

</div>
