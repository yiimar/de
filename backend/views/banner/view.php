<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Banner */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'width',
            'height',
            'class',
            [
                'attribute' => 'place_page_id',
                'value'     => HTML::encode(common\models\Page::getTitleById($model->place_page_id)),
            ],
            [
                'attribute' => 'target_place_id',
                'value'     => HTML::encode(common\models\Page::getTitleById($model->target_place_id)),
            ],
            'target',
            'status',
        ],
    ]) ?>
    <?php if (isset($model->pict)) {
        echo Html::img(
                Yii::getAlias('@bannerUpload/' . $model->pict), ['width' => '400px', 'height' => 'auto']
        );
    }
    ?>

</div>
