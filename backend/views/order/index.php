<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // echo Html::a('Создать заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'number',
            'fio',
            'total',
            'total_with',
            'created_at:datetime',
            'updated_at:datetime',
            'phone',
            'email:email',
//            'notes:ntext',
            [
                'attribute' => 'status',
                'filter'    => Order::statusForDrop(),
            ],
            [
                'class'         => 'yii\grid\ActionColumn',
                'header'        => 'Действия', 
                'headerOptions' => ['width' => '40'],
                'template'      => '{view} {update}',// {delete}
            ],
        ],
    ]); ?>

</div>
