<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\Category;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->title;

$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Товар: ' . $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот товар?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="row">
        <div class="col-lg-5">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'slug',
                    'description:ntext',
                    [
                        'label' => HTML::encode('Серия косметики'),
                        'value' => HTML::encode(common\models\Category::getSeriaById($model->category_id)),
                    ],
                    [
                        'attribute' => 'price',
                        'format' => 'integer',
                    ],
                    'effect',
                    'short',
                    'bulk',
                    'advice',
                    'meta_title',
                    'meta_keyword',
                    'meta_description',
                    [
                        'attribute' => 'advice_id',
                        'value' => HTML::encode(Product::getTitleByID($model->advice_id)),
                    ],
//            'slug',
//            'photo',
                ],
            ]) ?>
        </div>
    </div>
    <?php
    if (isset($model->photo)) {
        echo Html::img(
                Yii::getAlias('@productUpload/' . $model->photo), ['width' => '200px', 'height' => 'auto']
        );
    }
    ?>

</div>
