<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">
    <div class="row">
        <div class="col-lg-5">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

            <?php echo $form->field($model, 'file')->fileInput() ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(common\models\Category::getSeriaList(), 'id', 'title'), ['prompt' => 'Выбор категории']) ?>

            <?= $form->field($model, 'price')->textInput(['maxlength' => 19, 'format' => 'integer']) ?>

            <?= $form->field($model, 'effect')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'short')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'bulk')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'advice')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'meta_keyword')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'meta_description')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'advice_id')->dropDownList(common\models\Product::getTitleList()) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div> 
</div>
