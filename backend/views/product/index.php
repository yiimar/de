<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\Category;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать Товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'id',
            'title',
            [
                'attribute' => 'photo',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return HTML::img(
                            Yii::getAlias('@productUpload/' . $model->photo),
                            ['width'=>'100px', 'height'=>'auto']
                    );
                },
            ],
//            'description:ntext',
            [
                'attribute' => 'category_id',
                'value'     => function ($model) {
                    return empty($model->category_id) ? '-' : HTML::encode(Category::getSeriaById($model->category_id));
                },
                'filter'    => Category::getSeriaArray(),
            ],
            [
                'attribute' => 'price',
                'format'    => 'integer',
            ],
            'effect',
            'short',
            'bulk',
            'advice',
//            'meta_title',
//            'meta_keyword',
//            'meta_description',
            [
                'attribute' => 'advice_id',
                'value'     => function ($model) {
                    return empty($model->advice_id) ? ' ' : HTML::encode(Product::getTitleByID($model->advice_id));
                },
            ],

            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',// {images}
                'buttons'  => [
                    'images' => function ($url, $model, $key) {
                         return Html::a('<span class="glyphicon glyphicon glyphicon-picture" aria-label="Image"></span>', Url::to(['image/index', 'id' => $model->id]));
                    }
                ],
            ],
        ],
    ]); ?>

</div>
