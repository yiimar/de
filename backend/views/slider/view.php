<?php

use Yii;
use yii\helpers\Html;
use yii\widgets\DetailView;

//use yii\helpers\Html;
//use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Slider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-view">
    <div class="row">
        <div class="col-lg-5">

            <h1><?php echo Html::encode($this->title) ?></h1>

            <p>
                <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php
                echo Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ])
                ?>
            <?php // echo $id  ?>
            </p>

            <?php
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'slide',
                    'page_id',
                    'status',
                ],
            ])
            ?>
        </div>  
    </div>
    <?php if (isset($model->slide)) {
        echo Html::img(
                Yii::getAlias('@sliderUpload/' . $model->slide), ['width' => '400px', 'height' => 'auto']
        );
    } ?>


