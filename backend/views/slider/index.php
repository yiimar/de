<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Slider;

/* @var $this yii\web\View */
/* @var $searchModel common\models\query\SliderQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::encode('Слайдер');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-index">

    <h1><?= $this->title ?></h1>

    <p>
        <?= Html::a(Html::encode('Создать слайд'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute' => 'slide',
                'format'    => 'raw',
                'value'     => function ($model) {
                    return HTML::img(
                            Yii::getAlias('@sliderUpload/' . $model->slide),
                            ['width'=>'400px', 'height'=>'auto']
                    );
                },
            ],
            'page_id',
            [
                'attribute' => 'status',
//                'filter'    => Slider::statusForDrop(),
            ],
 
//            ['class' => 'yii\grid\ActionColumn'],
            [
                'class'    => '\\backend\\components\\CrudActionColumn',
            ],
        ],
    ]); ?>

</div>
