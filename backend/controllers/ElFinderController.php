<?php

namespace backend\controllers;

use Yii;       
use yii\web\Controller;         
use zxbodya\yii2\elfinder\ConnectorAction; 

/**
 * Description of ElFinderController
 *
 * @author yiimar
 */
class ElFinderController extends \backend\components\BackendController         
{         
    public function actions()         
    {         
        return [         
            'connector' => array(         
                'class' => ConnectorAction::className(),         
                'settings' => array(         
                    'root'       => Yii::getAlias('@webroot') . '/upload/',                    
                    'URL'        => Yii::getAlias('@web')     . '/upload/',         
                    'rootAlias'  => 'Home',         
                    'mimeDetect' => 'none'         
                )                    
            ),         
        ];                    
    }         
}