<?php

namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use backend\components\AbstractCrudController;

/**
 * Description of BackendCrudController
 *
 * @author yiimar
 */
class CruduploadController extends AbstractCrudController
{
    /**
     * Creates a new Option model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->modelClass;

        if ($model->load(Yii::$app->request->post()) && ($model->validate())) {
            // get the instance of upload file
            $model->file  = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                //Set the path that the file will be uploaded to
                $model->pict = md5(time()) . '.' . $model->file->extension;

                // upload image to uploadPath
                $model->file->saveAs(Yii::getAlias(
                        '@'
                        . $this->kind
                        . 'UploadPath/'
                        . $model->pict
                ));
            }

            $model->save();

            return $this->redirect([$this->kind . '/index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Option model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && ($model->validate())) {
            // get the instance of upload file
            $model->file  = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
                //Set the path that the file will be uploaded to
                $model->pict = md5(time()) . '.' . $model->file->extension;

                // upload image to uploadPath
                $model->file->saveAs(Yii::getAlias(
                        '@'
                        . $this->kind
                        . 'UploadPath/'
                        . $model->pict
                ));
            }

            $model->save();

            return $this->redirect([$this->kind . '/index']);

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

}
