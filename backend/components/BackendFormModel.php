<?php

namespace backend\components;

use Yii;
use yii\base\Model;

/**
 * Description of BackendFormModel
 *
 * @author yiimar
 */
class BackendFormModel extends Model
{
    private $_sessionKey;
    private $_modelClass;
    public $redirect;

    /**
     * 
     */
    public function init($id = null)
    {        
        $session = Yii::$app->session;
        $this->_modelClass = $this->getModelClass($id);
        $this->_sessionKey = $this->getSessionKey();

        if ($this->load(Yii::$app->request->queryParams) && $this->validate()) :
            $session->set($this->getSessionKey(), $this->getAttributes());
            \Yii::$app->runAction([$this->redirect]);
        else :
            if (is_array($data = $session->get($this->getSessionKey()))) : 
                $this->setAttributes($data);
            endif;
        endif;
    }

    /**
     * @return type string of attributes data session key
     */
    public function getModelClass()
    {
        if (!isset($this->_modelClass)) :
            $this->_modelClass = $this->formName();
            $id = Yii::$app->request->queryParams['id'];

            if (isset($id))             $this->_modelClass = $this->findModel($id);
            
        endif;

        return $this->_modelClass;
    }

    /**
     * Finds the model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = $this->_modelClass;
        if (($model = $model::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return type string of attributes data session key
     */
    public function getSessionKey()
    {
        if (!isset($this->_sessionKey))         $this->_sessionKey = $this->_modelClass . 'SearchFormData';

        return $this->_sessionKey;
    }
}
