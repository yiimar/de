<?php
namespace backend\components;

use Yii;
use yii\web\NotFoundHttpException;
use backend\components\BackendController;

/**
 * Description of BackendCrudController
 *
 * @author yiimar
 */
abstract class AbstractCrudController extends BackendController
{
    /**
     * Application base models path
     */
    public $modelPath;
    /**
     * Base string for defind kind of the CRUD
     * @var type string
     */
    private $_kind;

    /**
     * Class options init
     */
    public function init($modelPath = null)
    {
        $this->modelPath = $modelPath ? $modelPath : '\\common\\models\\';
        $this->setKind();
    }

    /**
     * CRUD kind setter
     * @return type string
     */
    public function setKind()
    {
        return $this->_kind = Yii::$app->request->get('kind');
    }

    /**
     * CRUD kind getter
     * @return type string
     */
    public function getKind()
    {
        return $this->_kind;
    }

    /**
     * Model class getter
     * @return type string
     */
    public function getModelClass()
    {
        return $this->modelPath . ucfirst($this->kind);
    }

    public function getPrePath()
    {
        return DIRECTORY_SEPARATOR . $this->kind . DIRECTORY_SEPARATOR;
    }

    /**
     * Finds the Option model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Option the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $modelClass = new $this->modelClass;
        
        if (($model = $modelClass::findOne($id)) !== null)   return $model;
        else                                                throw new NotFoundHttpException('Такой страницы не существует');
    }

    /**
     * Lists all Option models.
     * @return mixed
     */
    abstract public function actionIndex();

    /**
     * Creates a new Option model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    abstract public function actionCreate();

    /**
     * Updates an existing Option model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    abstract public function actionUpdate($id);

}
