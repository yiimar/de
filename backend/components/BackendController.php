<?php

namespace backend\components;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Description of BackendController
 *
 * @author yiimar
 */
class BackendController  extends Controller
{
    public $enableCsrfValidation = true;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'verbs' => ['GET', 'POST'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['error'],
                        'roles'   => ['?']
                    ],
                    [
                        'allow' => true,
//                        'controllers' => ['user'],
                        'actions'     => [
                            'login',
                            'register',
                            'password-reset-request',
                            'password-reset',
                        ],
                        'verbs'       => ['GET', 'POST'],
                        'roles'       => ['?'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}
