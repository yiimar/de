<?php

namespace backend\components;

use Yii;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/**
 * Description of CrudActionColumn
 *
 * @author yiimar
 */
class CrudActionColumn extends ActionColumn
{
    public function init()
    {
        $this->controller    = Yii::$app->request->get('kind');
        $this->header        = 'Действия';
        $this->headerOptions = ['width' => '40'];
        $this->template      = '{update} {delete}';
        $this->buttons       = [
            'update' => function ($url, $model) {
                return Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        ['/' . $this->controller . '/update/' . $model->id]
                );
            },
            'delete' => function ($url, $model) {
                return Html::a(
                        '<span class="glyphicon glyphicon-trash"></span>',
                        ['/' . $this->controller . '/delete/' . $model->id],
                        [
                            'data' => [
                                'method'  => 'post',
                                'confirm' => 'Вы действительно хотите удалить?',
                                'params'  => ['id' => $model->id],
                            ],
                        ]
                );
            },
            
        ];
    }
}
