<?php

use common\models\Category;

return [
    [
        'label' => 'Палантины',
        'url'   => '/palantiny/',
        'class' => 'item'
    ],
    [
        'label' => Category::PASHMINA,
        'url'   => '/' . Category::PASHMINA_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => Category::SILK,
        'url'   => '/' . Category::SILK_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => Category::CASHMERE,
        'url'   => '/' . Category::CASHMERE_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => Category::WOOL,
        'url'   => '/' . Category::WOOL_SLUG . '/',
        'class' => 'item'
    ],
    [
        'label' => 'Контакты',
        'url'   => '/kontaktnaya-informatsiya/',//
        'class' => 'last',
    ],
];
