<?php

use common\models\Category;

return [
    [
        'label' => 'Доставка',
        'url'   => '/dostavka/',
        'class' => 'item'
    ],
    [
        'label' => 'Оплата',
        'url'   => '/palantiny/',
        'class' => 'item'
    ],
    [
        'label' => 'Контакты',
        'url'   => '/kontaktny/',//
        'class' => 'last',
    ],
];
