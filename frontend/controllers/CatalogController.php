<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Product;
use frontend\components\FrontendController;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class CatalogController extends FrontendController
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }

    public function actionCategory($category)
    {
        $seria = Category::makeSlug($category);

        $productsQuery = Product::find()
                ->where(['category_id' => $seria[0]])
                ->orderBy('id')
                ->all();

        $productsDataProvider = new ActiveDataProvider([
            'query' => $productsQuery,
        ]);

        return $this->render('category', [
            'title'         => $seria[0],
            'productsQuery' => $productsQuery,
        ]);
    }

    public function actionView($slug)
    {
        $model = Product::getProductBySlug($slug);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    public function actionView1($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
