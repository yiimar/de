<?php
namespace frontend\controllers;

use Yii;
use frontend\components\FrontendController;
use frontend\models\forms\SubscribeForm;
use frontend\models\forms\CallbackForm;
use common\models\Page;

use yii\web\HttpException;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'landing';
        
        $text = Page::getText('Главная');
        return $this->render('index', ['text' => $text]);
    }

    /**
     * Re- Subscribe action
     * @param type integer $id
     * @return type mixeds
     */
    public function actionResub($id)
    {
        $res = Subscribe::resub($id);
        return $this->render('resub', ['res' => $res,]);
    }
    
    /**
     * News subscription action
     * @return SubscribeForm
     * @throws HttpException
     */
    public function actionSubscribe()
    {
        if (Yii::$app->request->isAjax)         return new SubscribeForm();
        else                                    throw new HttpException(404, 'Запрашиваемая страница не найдена');;
    }

    /**
     * Custom callback action
     * @return CallbackForm
     * @throws HttpException
     */
    public function actionCall()
    {
        if (Yii::$app->request->isAjax)         return new CallbackForm();
        else                                    throw new HttpException(404, 'Запрашиваемая страница не найдена');;
    }


    public function actionLeftbanner()
    {
        return $this->render('leftbanner');
    }

    public function actionRightbanner()
    {
        return $this->render('rightbanner');
    }
}
