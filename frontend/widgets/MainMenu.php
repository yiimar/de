<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

/**
 * Description of MainMenu
 *
 * @author yiimar
 */
class MainMenu extends Widget
{
    public $headerMenu;

    public function init()
    {
        parent::init();
        $this->headerMenu = require (Yii::$app->basePath . '/menu/headerMenu.php');
    }

    public function run()
    {
        return $this->render('mainMenu', ['headerMenu' => $this->headerMenu]);
    }
}
