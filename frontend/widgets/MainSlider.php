<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use common\models\Slider;

/**
 * Description of MainSlider
 *
 * @author yiimar
 */
class MainSlider extends Widget
{
    public $slider;
    
    public function init()
    {
        parent::init();
        $this->slider = Slider::find()
                ->where(['status' => Slider::STATUS_ACTIVE])
                ->orderBy('id')
                ->all();
    }

    public function run()
    {
        return $this->render('mainSlider', ['slider' => $this->slider]);
    }
}
