<?php /* */ ?>

<div class="slide-holder">
    <div id="carousel-debora" class="carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
            <?php for ($i = 0; $i < count($slider); $i++) : ?>
                <li 
                    data-target="#carousel-debora"
                    data-slide-to="<?php echo $i?>"
                    <?php echo ($i==0) ? 'class="active"' : '' ?>
                ></li>
            <?php endfor ?>
        </ol>

        <div class="carousel-inner" role="listbox">
            <?php for ($i = 0; $i < count($slider); $i++) : ?>
                <div <?php echo ($i==0) ? 'class="item active"' :  'class="item"'?>>
                    <img 
                        src="<?php echo Yii::getAlias('@sliderUpload/' . $slider[$i]['slide'])?>"
                        alt="<?php echo $slider[$i]['title']?>"
                        width="100%"
                    >
                </div>
            <?php endfor ?>
        </div>

        <a class="left carousel-control" href="#carousel-debora" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-debora" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
