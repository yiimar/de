<?php

use yii\helpers\Html;
?>

<div class="main-menu">
    <div class="container">
        <div class="collapse navbar-collapce" id="mobile-menu">
            <ul class="nav navbar-nav">
                
                <?php foreach ($headerMenu as $item) : ?>
                    <li>
                        <a href="<?php echo $item['url'] ?>">
                            <?php echo HTML::encode($item['label'])?>
                        </a>
                    </li>
                <?php endforeach ?>
                    
            </ul>
        </div>
    </div><!--container-->
</div>