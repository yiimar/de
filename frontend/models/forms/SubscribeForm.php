<?php

namespace frontend\models\forms;

use Yii;
use yii\base\Model;
use yii\web\Response;
use common\models\Subscribe;
use common\models\Coupon;

/**
 * SubscribeForm is the model behind the contact form.
 */
class SubscribeForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    public function init()
    {
        parent::init();

        $data = Yii::$app->request->post();

            $email = explode(":", $data['email']);
            $email  = $email[0];
            
            Yii::$app->response->format = Response::FORMAT_JSON;

            if (Subscribe::findOne(['email' => $email])) {
                return [
                    'code' => 100,
                    'str'  => 'Ваш Email уже подписан',
                ];
            } else {
                $model = new Subscribe();
                $model->code   = Coupon::makeCode(1, $model->email);
                $model->status = Subscribe::STATUS_ACTIVE;
                $model->email  = $email;

                if ($model->validate() && $model->save()) {
                    $model->sendEmail();
                    sleep(1);
                
                    return [
                        'code' => 100,
                        'str' => $email . ' успешно подписан',
                    ];
                }
            }

    }
}
