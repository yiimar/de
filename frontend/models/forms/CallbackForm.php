<?php

namespace frontend\models\forms;

use Yii;
use yii\base\Model;
use yii\web\Response;
use common\models\Callback;

/**
 * CallbackForm is the model behind the contact form.
 */
class CallbackForm extends Model
{
    public $phone;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone',],            'required'],
            [['phone'],             'string',    'max' => 20,],
        ];
    }

    public function init()
    {
        parent::init();

        $data = Yii::$app->request->post();

        $phone = explode(":", $data['phone']);
        $model = new Callback;
        $model->phone = $phone[0];

        if (($model->validate()) && $model->save(false)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return  ['code'   => 100,];
        }
    }
}
