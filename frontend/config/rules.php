<?php

return [
    
    '/' => 'site/index',

    ['pattern' => '/otpiska-ot-rassylki/<id:\d+>',  'route' => 'site/resub',        'suffix' => '/'],
    ['pattern' => '/site/call',                     'route' => 'site/call',         'suffix' => '/'],
    ['pattern' => '/site/subscribe',                'route' => 'site/subscribe',    'suffix' => '/'],
    ['pattern' => '/leftbanner',                    'route' => 'site/leftbanner',   'suffix' => '/'],
    ['pattern' => '/rightbanner',                   'route' => 'site/rightbanner',  'suffix' => '/'],


    ['pattern' => '/vasha-korzina/',                'route' => 'cart/order',        'suffix' => '/'],
    ['pattern' => '/vasha-korzina/<promo:\d+>',     'route' => 'cart/order',        'suffix' => '/'],
    ['pattern' => '/vasha-rebate/<id:\d+>',         'route' => 'cart/rebate',       'suffix' => '/'],
    ['pattern' => '/korzina-izmen',                 'route' => 'cart/update',       'suffix' => '/'],
    ['pattern' => '/korzina-promo',                 'route' => 'cart/promo',        'suffix' => '/'],
    ['pattern' => '/cart/add/<id:\d+>',             'route' => 'cart/add',          'suffix' => '/'],
    ['pattern' => '/cart/remove/<id:\d+>',          'route' => 'cart/remove',       'suffix' => '/'],
    ['pattern' => '/privetstvie/<zakaz:\d+>',       'route' => 'cart/advert',       'suffix' => '/'],

    ['pattern' => '/pashmina/<slug:[-a-zA-Z]+>',    'route' => 'catalog/view',    'suffix' => '.html'],
    ['pattern' => '/shelk/<slug:[-a-zA-Z]+>',       'route' => 'catalog/view',    'suffix' => '.html'],
    ['pattern' => '/kashemir/<slug:[-a-zA-Z]+>',    'route' => 'catalog/view',    'suffix' => '.html'],
    ['pattern' => '/kashemir/<slug:[-a-zA-Z]+>',    'route' => 'catalog/view',    'suffix' => '.html'],

    ['pattern' => '/o-kompanii',                    'route' => 'page/about',        'suffix' => '/'],
    ['pattern' => '/dostavka-i-oplata',             'route' => 'page/ship',         'suffix' => '/'],
    ['pattern' => '/kontaktnaya-informatsiya',      'route' => 'page/contact',      'suffix' => '/'],
    ['pattern' => '/konfidentsialnost',             'route' => 'page/confidence',   'suffix' => '/'],
    ['pattern' => '/usloviya-ispolzovaniya',        'route' => 'page/terms',        'suffix' => '/'],

//    ['pattern' => '/o-kompanii/<slug:[-a-zA-Z]+>',                  'route' => 'page/view',     'suffix' => '/'],//
//    ['pattern' => '/dostavka-i-oplata/<slug:[-a-zA-Z]+>',           'route' => 'page/view',     'suffix' => '/'],//
//    ['pattern' => '/kontaktnaya-informatsiya/<slug:[-a-zA-Z]+>',    'route' => 'site/view',     'suffix' => '/'],//
//    ['pattern' => '/konfidentsialnost/<slug:[-a-zA-Z]+>',           'route' => 'page/view',     'suffix' => '/'],//
//    ['pattern' => '/usloviya-ispolzovaniya/<slug:[-a-zA-Z]+>',      'route' => 'page/view',     'suffix' => '/'],//

    ['pattern' => '/<category:[-a-zA-Z]+>',         'route' => 'catalog/category',  'suffix'  => '/'],

//    ['pattern' => '/', 'route' => '', 'suffix' => '/'],
];
