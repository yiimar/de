<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'Debora Cosmetics',
    
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'        => 'site/index',
    
    'bootstrap' => ['log'],

    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
//            'suffix' => '/',
            'rules'           => require (__DIR__ . '/rules.php'),
        ],
        'request' => [
            'baseUrl' => '',
        ],
        'user'    => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name' => '_frontendUser', // unique for frontend
            ]
        ],
        'mailer' => [
            'class'             => 'yii\swiftmailer\Mailer',
            'viewPath'          => '@common/mail',
            'useFileTransport'  => false,
            'transport'     => [
                'class'     => 'Swift_SmtpTransport',
                'host'      => 'smtp.yandex.ru',//ssl://smtp.yandex.ru // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username'  => 'debora-cosmetics@yandex.ru',
                'password'  => 'seoakb',
                'port'      => '587',
                'encryption'=> 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
        ],
    ],
    'params' => $params,
];
