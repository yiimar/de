<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\models\Option;
use frontend\widgets\MainMenu;

?>

<header>
    <div class="container">
        <div class="row">
            <div class="phone-block col-lg-3">
                <?php
                
                $option = Option::findOne(['status' => Option::STATUS_ACTIVE, 'attribute' => 'Обратный звонок']);
                $phone  = $option ?$option->value :'';
                $option = Option::findOne(['status' => Option::STATUS_ACTIVE, 'attribute' => 'Обратный звонок Подпись']);
                $link   = $option ?$option->value :'';
                ?>
                <div class="phone-num"><?= HTML::encode($phone); ?></div>
                <div><a class="callback" href="#" data-toggle="modal" data-target="#Callback"><?php echo HTML::encode($link); ?></a></div>
            </div>
            <div class="col-lg-offset-1 col-lg-3">
                <div class="logo-block">
                    <?php // $logoFactor = isset($this->params['logoFactor']) ? $this->params['logoFactor'] : [],?>
                    <?php if ($this->params['logoLink'] === true) : ?><a href="/"><?php endif;// echo $this->title; ?>
                        <div>
                            <b><i><?php echo 'devvi.ru' ?></i></b><br>
                            <span style="font-size: 20px; line-height: 1.0em"><?= 'магазин палантинов' ?></span>
                        </div>
                    <?php if ($this->params['logoLink'] === true) : ?></a><?php endif; ?>
                </div>
            </div>
            <div class="col-lg-3">
                <?php $topMenu = require (Yii::$app->basePath . '/menu/topMenu.php'); ?>

                <ul class="top-menu nav navbar-nav">
                    <?php foreach ($topMenu as $item) : ?>
                        <li class="<?= $item['class']; ?>">
                            <?php
                            if (isset($item['url']))
                                echo HTML::a(HTML::encode($item['label']), $item['url']);
                            else
                                echo HTML::encode($item['label']);
                            ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-lg-2">
                <?php
                $cart = \Yii::$app->cart; 
                    $num = 0;
                    if ($cart->getIsEmpty()) :
                        $res = HTML::encode('В корзине нет товаров');
                    else :
                        $num = $cart->getCount();
                        $sum = $cart->getCost();
                        $hvost = $num % 10;
                        switch ($hvost) :
                            case 1:
                                $hvost = '';
                                break;
                            case 2:
                            case 3:
                            case 4:
                                $hvost = 'а';
                                break;
                            default :
                                $hvost = 'ов';
                                break;                           
                        endswitch;
                        
                        if (in_array($num, [11,12,13,14,15,16,17,18,19]))   {$hvost='ов';}
                                
                        $res = HTML::encode($num . ' товар' . $hvost . ' на ' . $sum . ' р.');
                    endif;
                ?>
            <div class="cart-block">
                <a <?= ($cart->getIsEmpty()) ? 'onClick="return false"' : ''; ?> href="<?php echo Yii::$app->request->baseUrl . '/vasha-korzina/' ?>">
                    <?php echo Html::img('@web/images/bag.png', ['class' => 'basket-small']) ?>

                    <div class="cart-q"  <?= ($cart->getIsEmpty()) ? 'style="display:none;"' : '' ?>>
                        <div class="q-number"><?php echo $num; ?></div>
                    </div>

                    <div class="cart-text">
                        <?= $res ?>
                    </div>
                </a>
            </div>
                
            </div>
        </div>
<!--                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="top-menu-block">
            </div>
        </div>-->
    </div>
</header>

<?php echo MainMenu::widget(); ?>
