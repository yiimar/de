<?php
use yii\helpers\Html;
?>
<div class="modal fade" id="Callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button id="callback-close" type="button" class="close" data-dismiss="modal" aria-label="Close"><?= Html::encode('Нет, спасибо ') ?><span aria-hidden="true"><?= Html::encode('×') ?></span></button>
                <h4 class="modal-title" id="CallbackLabel"><?= Html::encode('Заказ обратного звонка') ?></h4>
            </div>
            <div class="modal-body">
                <div class="podbor-l-col">

                </div>
                <div class="podbor-r-col">
                    <p><?= HTML::encode('Напишите номер телефона и мы позвоним в течение часа.') ?></p>
                    <div class="input-group">
                        <input id="callback-phone" class="form-control search" placeholder="<?= Html::encode('+7 (_ _ _)') ?>" type="tel">
                        <span class="input-group-btn">
                            <button id="callback-button" class="btn btn-default search-button"><?= Html::encode('Отправить') ?></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>        
