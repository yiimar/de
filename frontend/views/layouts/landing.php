<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?= Html::csrfMetaTags() ?>
        <?php // if (isset($this->params['title'])) : ?>
            <title><?= Html::encode($this->title) ?></title>
        <?php // else: ?>
            <title><?php echo Html::encode(Yii::$app->id) ?></title>
        <?php // endif ?>
        <?php if (isset($this->params['description'])) : ?>
            <meta name="description" content="<?= Html::encode($this->params['description']) ?>" />
        <?php endif ?>
        <?php if (isset($this->params['keywords'])) : ?>
            <meta name="keywords"    content="<?= Html::encode($this->params['keywords']) ?>" />
        <?php endif ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

            <?php echo $this->render('_header');  ?>
            <div class="wrapper">
                <div class="content">
                    <?php echo $content ?>
                </div>
            </div>
            <?php echo $this->render('_footer');  ?>

            <?php echo $this->render('_callbackModal');  ?>

        <?php $this->endBody() ?>
    </body>
</html>

<?php $this->endPage() ?>