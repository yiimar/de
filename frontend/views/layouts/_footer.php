<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use common\models\Soclink;

?>

<div class="footer">
    <div class="container">
        <div class="footer-links">

            <?php $footerMenu = require (Yii::$app->basePath . '/menu/footerMenu.php'); ?>

            <ul>
                <?php foreach ($footerMenu as $item) : ?>
                    <li class="<?= $item['class']; ?>">
                        <?php
                        if (isset($item['url']))
                            echo HTML::a(HTML::encode($item['label']), $item['url']);
                        else
                            echo HTML::encode($item['label']);
                        ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="socials-holder">
            <?php $footerSocial = Soclink::find()->where(['status' => Soclink::STATUS_ACTIVE])->all() ?>
            <div class="socials">
                <?php foreach ($footerSocial as $item) : ?>
                    <a href="<?= $item->url ?>">
                        <div class="<?= $item->class ?>"></div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="copy"><?php echo HTML::encode('© Все права защищены.'); ?></div>
    </div>
</div>
