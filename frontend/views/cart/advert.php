<?php

use yii\helpers\Html;
use common\models\Order;

$this->title = Html::encode('Заказ оформлен! Номер заказа ' . $zakaz); //
//$this->params['breadcrumbs'][] = ['label' => HTML::encode(Category::getSeriaById($model->category_id)), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['logoLink'] = true;
?>

<h1><?= HTML::encode($this->title) ?></h1>

<div class="thanx-page">
    <div class="thanx-block">
        <p>Спасибо за заказ!</p>
    </div>
</div>
<br>
