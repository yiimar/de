<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Page;
use frontend\widgets\MainMenu;
use frontend\widgets\MainSlider;

$this->title = HTML::encode('Главная');
// Убираем ссылку с главного логотипа
$this->params['logoLink'] = false;
?>

<?php echo MainSlider::widget(); ?>

<div class="container">
    <?php echo $text; ?>

    <?php echo $this->render('_banners'); ?>			
    <?php // echo $this->render('_sign'); ?>

        <?php $this->registerJsFile(
                'js/index.js', ['depends' => 'frontend\assets\AppAsset']
        ); ?>

</div>