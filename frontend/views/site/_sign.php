<?php

use Yii;
use yii\helpers\Html;
?>

<div class="sign-block">
    <span></span>
    <div class="sign-block-text">
        <div class="sign-text"><?php echo HTML::encode('Подпишитесь на новости и получите скидку 10% на Ваш первый заказ:'); ?></div>
    </div>

    <input  id="subs-email" type="email" placeholder="<?php echo HTML::encode('Ваш email') ?>">
    <button id="subs-button" class="dbr-btn"><?php echo HTML::encode('ПОДПИСАТЬСЯ') ?></button>
</div>