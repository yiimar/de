<?php
use Yii;
use yii\helpers\Html;
use common\models\Banner;
use common\models\Page;

?>

<div class="main-buttons">
    <?php
    $bannerPath = Yii::getAlias('@bannerUpload/');
    $id = Page::getIdByTitle('Главная');
    if ($id) :
        
        $leftBanner = Banner::findOne([
            'title'         => 'Левый',
            'status'        => 'Показать',
            'place_page_id' => $id,
        ]);
        $rightBanner = Banner::findOne([
            'title'         => 'Правый',
            'status'        => 'Показать',
            'place_page_id' => $id,
        ]); 

        if ($leftBanner && $rightBanner) :
    ?>

            <div class="main-buttons-l">
                <a href="<?=$leftBanner->target?>">
                    <img 
                        class="<?= $leftBanner->class ?>"
                        src="<?= $bannerPath . $leftBanner->pict ?>"
                    >
                </a>
            </div>
            <div class="main-buttons-r">
                <a href="<?=$rightBanner->target?>">
                    <img 
                        class="<?=$rightBanner->class?>"
                        src="<?= $bannerPath . $rightBanner->pict?>"
                    >
                </a>
            </div>

        <?php endif; ?>
    <?php endif; ?>
    
</div>


