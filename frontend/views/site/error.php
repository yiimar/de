<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        <?=HTML::encode('К сожалению, Вы вызвали несуществующую страницу.')?>
    </p>
    <p>
        <?=HTML::encode('Попробуйте еще раз или перейдите на ')?><a href="/"><?=HTML::encode('главную страницу сайта.')?></a>
    </p>

</div>
