<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Html::encode($model->title);

$this->params['breadcrumbs'][] = $this->title;
$this->params['logoLink']      = true;

if (isset($model->meta_title))          $this->params['title'] = $model->meta_title;
else                                    $this->params['title'] = $model->title;

$this->params['keywords']    = $model->meta_keyword;
$this->params['description'] = $model->meta_description;

?>


<div class="container">

    <?php echo $this->render('_nav', ['id' => $model->id]); ?>

    <h1><?= $this->title ?></h1>

    <?php echo $model->text; ?>

    <?php echo $this->render('_map', ['page' => $model->id]); ?>

    <br>
</div>