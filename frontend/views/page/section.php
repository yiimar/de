<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use common\models\Yamap;

if ($model->meta_title)             $this->title = $model->meta_title;
else                                $this->title = $model->title;

$this->params['keywords']    = $model->meta_keyword;
$this->params['description'] = $model->meta_description;

$this->params['breadcrumbs'][] = $this->title;
$this->params['logoLink']      = true;

?>

    <?php echo $this->render('_nav', ['id' => $model->id]); ?>

    <h1><?= Html::encode($this->title ) ?></h1>

    <?php echo $model->text; ?>

    <?php // if ($map = Yamap::findOne(['status' => Yamap::STATUS_ACTIVE, 'page_id' => $model->id])) : ?>
        <?php // echo $this->render('_map', ['page' => $model->id]); ?>
    <?php // endif ?>
    <br>
