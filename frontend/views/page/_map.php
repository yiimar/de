<?php
use common\models\Yamap;
/**
 * Заглушка- аналог выборки из таблицы {{%map}}
 * 
 */
$map = Yamap::findOne(['status' => Yamap::STATUS_ACTIVE, 'page_id' => $page]);
//$map = [
//    'sid' => 'z6HOoVJ4qHxERIcO-mKLWjfrNmYHSWhL',
//    'width' => 100,
//    'height' => 400,
//    'sourceType' => 'constructor',    
//];
?>
<script 
    type="text/javascript"
    charset="utf-8"
    src="<?=$map->url?>?sid=<?=$map->sid?>&width=<?=$map->width?>%&height=<?=$map->height?>&lang=ru_RU&sourceType=<?=$map->source_type?>">
</script>



