<?php

use yii\helpers\Html;
use common\models\Page;

$items = Page::makeItemsForId($id);
$slug  = Page::getSlugById($id);
?>
<?php if (!empty($items)) : ?>
    <div class="podrazdel">
        <?php foreach ($items as $item) : ?>
            <a 
                style="padding-top: 10px;"
                href="<?php echo Yii::$app->request->baseUrl . '/' . $slug . '/' . $item->slug . '/' ?>"
            >
                <?php echo Html::encode($item->title) ?>
            </a> 
        <?php endforeach; ?>
    </div>
<?php endif; ?>
